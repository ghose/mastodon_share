#Mastodon Share

1.2.5

* Eliminado webservice al submitar
* Añadido Meneame
* Añadidos Pleroma y Nextcloud Bookmarks para futuro script, quedan bloqueados temporalmente
* Internacionalización con selector de lenguaje
* Añadidos catalán y español

1.2.4

* Añadido parametro url del webservice para actualizar clicks en la base de datos

1.2.3

* Refactorización del Javascript
* Mejorada la estructura con directorios
* Añadido Manifest.webmanifest
* Añadida versión del software
* Corregido error con ventana de Hubzilla

