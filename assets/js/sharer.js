/*! sharer v1.2.3 | (c) Copyleft 2018 | GNU/GPL v3 */ 
/*    
@licstart  The following is the entire license notice for the 
JavaScript code in this page.

Copyright (C) 2018  Kim

The JavaScript code in this page is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License (GNU GPL) as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.  The code is distributed WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

As additional permission under GNU GPL version 3 section 7, you
may distribute non-source (e.g., minimized or compacted) forms of
that code without the copy of the GNU GPL normally required by
section 4, provided you include this license notice and a URL
through which recipients can access the Corresponding Source.   


@licend  The above is the entire license notice
for the JavaScript code in this page.
*/

//configuration
var w = 650; //modal window width
var h = 500; //modal window height
var language = 'ca_ES'; //default language
		
function GetURLParameter(sParam)
{
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++)
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam)
		{
		    return sParameterName[1];
		}
	} 
}

$(document).ready(function() {

	//set language
	$('#lang').change(function() {
		var language = $(this).val();
		console.log(language);
		$.i18n.properties({ 
			name: 'Messages', 
			path: 'assets/js/', 
			mode: 'both', 
			language: language, 
			callback: function() { 
				$("#msg_thanks").text($.i18n.prop('msg_thanks')); 
				$("#msg_hubzilla_title").text($.i18n.prop('msg_hubzilla_title')); 
				$("#msg_hubzilla_title").text($.i18n.prop('msg_hubzilla_title')); 
				$("#msg_hubzilla_desc").text($.i18n.prop('msg_hubzilla_desc')); 
				$("#hostHubzilla").attr('placeholder', $.i18n.prop('hostHubzilla')); 
				$("#textHubzilla").attr('placeholder', $.i18n.prop('textHubzilla')); 
				$("#msg_hubzilla_remember").text($.i18n.prop('msg_hubzilla_remember')); 
				$("#msg_hubzilla_submit").text($.i18n.prop('msg_hubzilla_submit')); 
				$("#msg_diaspora_title").text($.i18n.prop('msg_diaspora_title')); 
				$("#msg_diaspora_desc").text($.i18n.prop('msg_diaspora_desc')); 
				$("#hostDiaspora").attr('placeholder', $.i18n.prop('hostDiaspora')); 
				$("#textDiaspora").attr('placeholder', $.i18n.prop('textDiaspora')); 
				$("#msg_diaspora_remember").text($.i18n.prop('msg_diaspora_remember')); 
				$("#msg_diaspora_submit").text($.i18n.prop('msg_diaspora_submit'));			
				$("#msg_friendica_title").text($.i18n.prop('msg_friendica_title')); 
				$("#msg_friendica_desc").text($.i18n.prop('msg_friendica_desc')); 
				$("#hostFriendica").attr('placeholder', $.i18n.prop('hostFriendica')); 
				$("#textFriendica").attr('placeholder', $.i18n.prop('textFriendica')); 
				$("#msg_friendica_remember").text($.i18n.prop('msg_friendica_remember')); 
				$("#msg_friendica_submit").text($.i18n.prop('msg_friendica_submit'));
				$("#msg_gnusocial_title").text($.i18n.prop('msg_diaspora_title')); 
				$("#msg_gnusocial_desc").text($.i18n.prop('msg_diaspora_desc')); 
				$("#hostGnusocial").attr('placeholder', $.i18n.prop('hostDiaspora')); 
				$("#textGnusocial").attr('placeholder', $.i18n.prop('textDiaspora')); 
				$("#msg_gnusocial_remember").text($.i18n.prop('msg_diaspora_remember')); 
				$("#msg_gnusocial_submit").text($.i18n.prop('msg_diaspora_submit'));
				$("#msg_mastodon_title").text($.i18n.prop('msg_diaspora_title')); 
				$("#msg_mastodon_desc").text($.i18n.prop('msg_diaspora_desc')); 
				$("#hostMastodon").attr('placeholder', $.i18n.prop('hostDiaspora')); 
				$("#textMastodon").attr('placeholder', $.i18n.prop('textDiaspora')); 
				$("#msg_mastodon_remember").text($.i18n.prop('msg_diaspora_remember')); 
				$("#msg_mastodon_submit").text($.i18n.prop('msg_diaspora_submit'));
				$("#msg_pleroma_title").text($.i18n.prop('msg_diaspora_title')); 
				$("#msg_pleroma_desc").text($.i18n.prop('msg_diaspora_desc')); 
				$("#hostPleroma").attr('placeholder', $.i18n.prop('hostDiaspora')); 
				$("#textPleroma").attr('placeholder', $.i18n.prop('textDiaspora')); 
				$("#msg_pleroma_remember").text($.i18n.prop('msg_diaspora_remember')); 
				$("#msg_pleroma_submit").text($.i18n.prop('msg_diaspora_submit'));
				$("#msg_meneame_title").text($.i18n.prop('msg_diaspora_title')); 
				$("#msg_meneame_desc").text($.i18n.prop('msg_diaspora_desc')); 
				$("#hostMeneame").attr('placeholder', $.i18n.prop('hostDiaspora')); 
				$("#textMeneame").attr('placeholder', $.i18n.prop('textDiaspora')); 
				$("#msg_meneame_remember").text($.i18n.prop('msg_diaspora_remember')); 
				$("#msg_meneame_submit").text($.i18n.prop('msg_diaspora_submit'));
				$("#msg_nextcloud_title").text($.i18n.prop('msg_diaspora_title')); 
				$("#msg_nextcloud_desc").text($.i18n.prop('msg_diaspora_desc')); 
				$("#hostNextcloud").attr('placeholder', $.i18n.prop('hostDiaspora')); 
				$("#textNextcloud").attr('placeholder', $.i18n.prop('textDiaspora')); 
				$("#msg_nextcloud_remember").text($.i18n.prop('msg_diaspora_remember')); 
				$("#msg_nextcloud_submit").text($.i18n.prop('msg_diaspora_submit'));
			}
		});
	});	

	//get version from manifest
	$.getJSON('manifest.webmanifest', function(data) {
		$.each(data, function(index, element) {
		    $('#version').html(data.version);
		});
	});
	
	//get url parameter
	var varText = GetURLParameter('text');
	var varUrl  = GetURLParameter('url');
	var varId   = GetURLParameter('articleid');
	$('.text').val(decodeURIComponent(varText));
	$('.url').val(decodeURIComponent(varUrl));
	
	//get browser database
	var hubzilla  	= localStorage.getItem('Hubzilla');
	var diaspora  	= localStorage.getItem('Diaspora');
	var gnusocial 	= localStorage.getItem('Gnusocial');
	var mastodon  	= localStorage.getItem('Mastodon');
	var friendica  	= localStorage.getItem('Friendica');
	var pleroma     = localStorage.getItem('Pleroma');
	
	if(hubzilla != null) { 
		$('#hostHubzilla').val(hubzilla);
		$('.hubzilla').removeAttr('disabled');
	}
	if(diaspora != null) { 
		$('#hostDiaspora').val(diaspora);
		$('.diaspora').removeAttr('disabled');
	}
	if(gnusocial != null) { 
		$('#hostGnusocial').val(gnusocial);
		$('.gnusocial').removeAttr('disabled');
	}
	if(mastodon != null) { 
		$('#hostMastodon').val(mastodon);
		$('.mastodon').removeAttr('disabled');
	}
	if(friendica != null) { 
		$('#hostFriendica').val(friendica);
		$('.friendica').removeAttr('disabled');
	}
	if(pleroma != null) { 
		$('#hostPleroma').val(pleroma);
		$('.pleroma').removeAttr('disabled');
	}
	
	$('.host').keyup(function() {
		var service = $(this).closest("form").attr('data-service');
		
		if(service == 'Hubzilla') {
			$('.hubzilla').removeAttr('disabled');	
		}
		if(service == 'Diaspora') {	
			$('.diaspora').removeAttr('disabled');	
		}
		if(service == 'Gnusocial') {	
			$('.gnusocial').removeAttr('disabled');	
		}
		if(service == 'Mastodon') {	
			$('.mastodon').removeAttr('disabled');	
		}
		if(service == 'Friendica') {	
			$('.friendica').removeAttr('disabled');	
		}
		if(service == 'Pleroma') {	
			$('.pleroma').removeAttr('disabled');	
		}
	});
	
	//open new window
	$('.send').click(function(e) {
		e.preventDefault();
		
		var site  = $(this).attr('data-site');
		var check = $('#remember'+site);
		
		if(check.is(':checked')) {  
			var host = $('#host'+site).val();
			localStorage.setItem(site, host);
		}
		
	  	var text    = $('#text'+site).val();
	  	var host    = $('#host'+site).val();
	  	var url     = $('#url'+site).val();
	  	var action  = '';
	  	var service = $(this).closest("form").attr('data-service');

	  	if(host != '' && text != '') {
	  	
	  		//add protocol https
	  		var prefix = 'https://';
			if (host.substr(0, prefix.length) !== prefix)
			{
				host = prefix + host;
			}
		
			if(service == 'Hubzilla') {
				action = host+'/rpost?body='+encodeURIComponent(text)+'&url='+encodeURIComponent(url);	
				$('.hubzilla').removeAttr('disabled');					
			}
			if(service == 'Diaspora') {
				action = host+'/bookmarklet?url='+encodeURIComponent(url)+'&title='+encodeURIComponent(text)+'&jump=doclose';	
				$('.diaspora').removeAttr('disabled');	
			}
			if(service == 'Gnusocial') {
				action = host+'/?action=newnotice&status_textarea='+encodeURIComponent(text)+' '+encodeURIComponent(url);
				$('.gnusocial').removeAttr('disabled');	
			}
			if(service == 'Mastodon') {
				action = host+'/share?text='+encodeURIComponent(text)+'&url='+encodeURIComponent(url);	
				$('.mastodon').removeAttr('disabled');	
			}
			if(service == 'Friendica') {
				action = host+'/bookmarklet?url='+encodeURIComponent(url)+'&title='+encodeURIComponent(text)+'&jump=doclose';	
				$('.friendica').removeAttr('disabled');	
			}
			if(service == 'Pleroma') {
				action = host+'/notice/new?status_textarea='+encodeURIComponent(text)+'&url='+encodeURIComponent(url);
				$('.pleroma').removeAttr('disabled');	
			}
			if(service == 'Meneame') {
				action = host+'/login?return=/submit?url='+encodeURIComponent(url)+'&title='+encodeURIComponent(text);	
			}		
	  	}
		
		var left = Number((screen.width/2)-(w/2));
		var top  = Number((screen.height/2)-(h/2));

		window.open(action, '_blank', 'width='+w+',height='+h+',left='+left+',top='+top+'');	
	});
});
