#Free Social Sharer

Una sencilla página para compartir en redes sociales libres.

##uso

En tu sitio web crea un botón con la siguinte url yoursite.com/sharer/index.html/?text=tu texto&url=tu url donde text es el texto que deseés postear y url la url del post.

**Usa urlencode o encodeURIComponent dependiendo de si usas js o php para codificar esa variable, de lo contrario se rompera n las urls que usen ampersand**

##Licencia

Esta movida está escrita bajo la licencia gnu gpl v3 se libre de copiar, modificar y distribuir. Be water my friend ;)

##Traducciones

Los archivos de I18n (internacionalización) están en assets/js si por ejempo alguien quiere crear el euskera puede hacer un Messages_eu_ES.properties y será incluido en la próxima versión. ;)

##Donaciones

Es triste pedir pero peor es robar, si puedes colaborar con unos faircoins al proyecto se agradace: fcXgoJhkUYxKuHqQpC7vUK3LMMZpE5pu1x
